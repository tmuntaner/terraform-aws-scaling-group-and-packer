resource "aws_security_group" "main" {
  name        = "${terraform.workspace}-hello-world"
  description = "Hello World Security Group"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${terraform.workspace}-hello-world-sg"
  }
}

data "aws_region" "current" {}

data "aws_ami" "main" {
  most_recent = true

  filter {
    name   = "name"
    values = ["sles-15-rails-web-*"]
  }

  owners = ["374278911799"]
}

resource "aws_launch_configuration" "main" {
  lifecycle {
    create_before_destroy = true
  }

  image_id        = "${data.aws_ami.main.id}"
  name_prefix     = "hello-world"
  instance_type   = "t2.micro"
  key_name        = "${var.key_name}"
  security_groups = ["${aws_security_group.main.id}"]
}

resource "aws_security_group" "elb" {
  vpc_id = "${var.vpc_id}"

  lifecycle {
    create_before_destroy = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elb" "main" {
  name            = "hello-world"
  subnets         = ["${var.subnet_ids}"]
  security_groups = ["${aws_security_group.elb.id}"]

  listener {
    instance_port     = 3000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 10
    unhealthy_threshold = 2
    timeout             = 5
    interval            = 30
    target              = "HTTP:3000/healthz"
  }
}

resource "aws_autoscaling_group" "main" {
  lifecycle {
    create_before_destroy = true
  }

  name                 = "${aws_launch_configuration.main.name}"
  min_size             = "2"
  max_size             = "2"
  desired_capacity     = "2"
  launch_configuration = "${aws_launch_configuration.main.name}"
  vpc_zone_identifier  = ["${var.subnet_ids}"]
  load_balancers       = ["${aws_elb.main.name}"]
  health_check_type    = "ELB"
}
