resource "aws_vpc" "vpc" {
  cidr_block           = "${var.cidr_block}"
  enable_dns_hostnames = true

  tags {
    Name = "${terraform.env}-vpc"
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name = "${terraform.env}-gateway"
  }
}

resource "aws_subnet" "public" {
  count = "${length(var.azs)}"

  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${cidrsubnet(var.cidr_block, 5, count.index + 1)}"
  availability_zone       = "${element(var.azs, count.index)}"
  map_public_ip_on_launch = "true"

  tags {
    Name = "${terraform.env}-public-subnet-${element(var.azs, count.index)}"
  }
}

resource "aws_route_table" "public_routes" {
  vpc_id = "${aws_vpc.vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.main.id}"
  }

  tags {
    Name = "${terraform.env}-public-route-table"
  }
}

resource "aws_route_table_association" "subnet_route_table_assoc" {
  count = "${length(var.azs)}"

  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public_routes.id}"
}

resource "aws_eip" "gateway_eip" {
  count = "${length(var.azs)}"
  vpc   = "true"
}
