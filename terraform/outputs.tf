output "application_url" {
  value = "${module.cluster.elb_dns_name}"
}
